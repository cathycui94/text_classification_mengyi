from flask import Flask, render_template, request
import pickle
import pandas as pd
import numpy as np
from flask_wtf.csrf import CSRFProtect
from flask_wtf import FlaskForm
from keras.models import load_model
from wtforms import SubmitField, \
    StringField, FileField
from werkzeug.utils import secure_filename
import os

app = Flask(__name__)
csrf = CSRFProtect(app)

app.config.update(dict(
    SECRET_KEY="123",
    WTF_CSRF_SECRET_KEY="123"
))


class predict_form(FlaskForm):
    words = StringField("Words")
    # file = FileField("File")
    submit = SubmitField("Submit")


@csrf.exempt
@app.route('/', methods=['GET', 'POST'])
def index():
    form = predict_form(request.form)
    y = ''
    if request.method == 'POST':
        # file = form.file.data
        words = form.words.data
        # try:
        # if len(file.strip()) > 0:
        #
        #     filename = secure_filename(file.filename)
        #     file.save(filename)
        #
        #     with open(filename, 'rb') as temp_file:
        #         lines = temp_file.readlines()[0]
        #         print (lines)
        #         x = pd.Series(lines)
        #         x_train = vectorizer1.transform(x)
        #         y = get_result(x_train)
        #         print (x)
        #         if y in targets:
        #             y2 = get_result_lstm(x)
        #             if y2:
        #                 y = y2
        #
        # elif len(words.strip()) > 0:
        if len(words.strip()) > 0:
            x = pd.Series(words)
            x_train = vectorizer1.transform(x)
            y = get_result(x_train)
            if y in targets:
                y2 = get_result_lstm(x)
                if y2:
                    y = y2
        # except:
        #     y = 'Can not predict the class of your document!'

    return render_template('index.html', form=form, prediction=y)


"""
Get final result
"""


def get_result(x):
    a = ensemble_model['Perceptron']
    b = ensemble_model['Passive-Aggresive Classifier']
    c = ensemble_model['Rige Classifier']

    a_res = a.predict(x).tolist()[0]
    b_res = b.predict(x).tolist()[0]
    c_res = c.predict(x).tolist()[0]

    res = filter_cols(a_res, b_res, c_res)

    return res


"""
Filter result from 3 models
"""


def filter_cols(a, b, c):
    if a == b:
        return a
    elif a == c:
        return a
    elif b == c:
        return b
    else:
        return a


"""
For specific 4 types using lstm
"""


def get_result_lstm(x):
    NUM = 150
    sequence = vectorizer2.texts_to_sequences(x)
    x = sequence[0]
    # print ('the length is: ' + str(len(x)))
    if len(x) < NUM:
        return None
    x = x[:NUM]
    x = np.array(x)
    x = x.reshape((1, NUM))
    y = keras_model.predict(x)
    y_pred = np.argmax(y)
    y_class = label_encoder.inverse_transform(y_pred)
    # print ('the class is ' + y_class)
    return y_class


if __name__ == '__main__':
    with open('./model/vectorizer1.pkl', 'rb') as file:
        vectorizer1 = pickle.load(file)

    with open('./model/vectorizer2.pkl', 'rb') as file:
        vectorizer2 = pickle.load(file)

    with open('./model/label_encoder.pkl', 'rb') as file:
        label_encoder = pickle.load(file)

    with open('./model/ensemble_model.pkl', 'rb') as file:
        ensemble_model = pickle.load(file)

    keras_model = load_model('./model/keras_model.h5')

    targets = ['BILL', 'CHANGE ENDORSEMENT', 'RETURNED CHECK', 'POLICY CHANGE']

    app.run(host='localhost')
