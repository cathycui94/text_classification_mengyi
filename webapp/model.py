import numpy as np
import sklearn 
import pandas as pd
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
from sklearn.metrics import f1_score
import pickle
from sklearn.linear_model import RidgeClassifier
from sklearn.linear_model import ElasticNet
from sklearn.pipeline import Pipeline
from sklearn.svm import LinearSVC
from sklearn.linear_model import SGDClassifier
from sklearn.linear_model import Perceptron
from sklearn.linear_model import PassiveAggressiveClassifier
from sklearn.naive_bayes import BernoulliNB, MultinomialNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neighbors import NearestCentroid
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import BaggingClassifier
from sklearn.tree import DecisionTreeClassifier
# use keras and lstm for classification
from keras.preprocessing import sequence
from keras.preprocessing import text
from keras.models import Sequential
from keras.layers import Dense, Dropout, Embedding, LSTM, Bidirectional
from keras.datasets import imdb
from sklearn.preprocessing import LabelEncoder
from keras.utils.np_utils import to_categorical

heavy_water_data = pd.read_csv('./shuffled-full-set-hashed.csv', \
                            names=["doclabel", "words"])
# Load data set
y = heavy_water_data['doclabel']
x = heavy_water_data['words']
y = y.astype(str)
x = x.astype(str)

# Split train and test data sets
x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.3)

# Vectorize the words
vectorizer = TfidfVectorizer(sublinear_tf=True)
x_train = vectorizer.fit_transform(x_train)
x_test = vectorizer.transform(x_test)

# Target names
target_names = y.unique().tolist()
# store vectorizer
with open('vectorizer1.pkl', 'wb') as file:
    pickle.dump(vectorizer, file)


# Benchmark of classifier
def benchmark(clf, name):
    print ('_' * 50)
    print ('Training: {0}'.format(name))
    print (clf)
    
    model = clf.fit(x_train, y_train)
    y_pred = clf.predict(x_test)
    
#     Accuracy
    acc = accuracy_score(y_test, y_pred)
    print ('Accuracy: %.08f'%acc)
    
    f1 = f1_score(y_test, y_pred, average='macro')
    print ('F1: %.08f'%f1)
    
#   Classification report  
    print (classification_report(y_test, y_pred, target_names=target_names))
    
#     Confusion matrix
    print (confusion_matrix(y_test, y_pred))
      
    
    return model, acc, f1


# Add model to result
def add_model(result, name, model, acc, f1):
    if name not in result:
        result[name] = {}
    result[name]['model'] = model
    result[name]['accuracy'] = acc
    result[name]['f1'] = f1


# Ridge regression
name = 'Ridge Classifier'
model, acc, f1 = benchmark(RidgeClassifier(tol=1e-3), name)
add_model(result, name, model, acc, f1)

# Perceptron
name = 'Perceptron'
model, acc, f1 = benchmark(Perceptron(n_iter=100), name)
add_model(result, name, model, acc, f1)

# Passive aggressive 
name = "Passive-Aggressive"
model, acc, f1  = benchmark(PassiveAggressiveClassifier(n_iter=100), name)
add_model(result, name, model, acc, f1)

# Random forest
name = "Random forest"
model, acc, f1 = benchmark(RandomForestClassifier(n_estimators=5), name)
add_model(result, name, model, acc, f1)

# SGD using elastic net
name = "SGD elastic net"
model, acc, f1 = benchmark(SGDClassifier(alpha=.0001, n_iter=3, penalty="elasticnet"), name)
add_model(result, name, model, acc, f1)

# SVM classifier
name = "SVM classifier"
model, acc, f1 = benchmark(LinearSVC(penalty="elasticnet", tol=1e-3), name)
add_model(result, name, model, acc, f1)

# Naive Bayes
name = "Naive Bayes"
model, acc, f1 = benchmark(MultinomialNB(), name)
add_model(result, name, model, acc, f1)

# Bernouli
name = "Bernoulli Naive Bayes"
model, acc, f1 = benchmark(BernoulliNB(), name)
add_model(result, name, model, acc, f1)

# KNN 
name = "kNN"
model, acc, f1 = benchmark(KNeighborsClassifier(n_neighbors=10), name)
add_model(result, name, model, acc, f1)

# Ensemble models
def filter_cols(a, b, c):
    
    if a == b:
        return a
    elif a == c:
        return a
    elif b == c:
        return b
    else:
        return a
    
def ensemble_models(result, x_test, y_test):
    
    y_pred = {}
    
    keys = list(result.keys())
    
    for name in keys:
        
        model = result[name]['model']
        y_pred[name] = model.predict(x_test)
        
    y_pred_df = pd.DataFrame(y_pred)
    y_pred_ens = y_pred_df.apply(lambda row: filter_cols(row[keys[0]], row[keys[1]], row[keys[2]]), axis=1)
    y_pred_ens = np.array(y_pred_ens)
    
    print ('_' * 50)
    print ('Training: {0}'.format('Ensemble Method'))
    
#     Accuracy
    acc = accuracy_score(y_test, y_pred_ens)
    print ('Accuracy: %.08f'%acc)
    
    f1 = f1_score(y_test, y_pred_ens, average='macro')
    print ('F1: %.08f'%f1)
    
#   Classification report  
    print (classification_report(y_test, y_pred_ens, target_names=target_names))
    
#     Confusion matrix
    print (confusion_matrix(y_test, y_pred_ens))
    
    return y_pred_ens, acc, f1

# Get all result
y_pred, acc, f1 = ensemble_models(result, x_test, y_test)


maxlen = 150
batch_size = 32

print('Loading data...')

# Load data from x and y
x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.3)
print(x_train.shape[0], 'train sequences')
print(x_test.shape[0], 'test sequences')

# Tokenizer
print ('Tokenize data...')
tokenizer = text.Tokenizer()
tokenizer.fit_on_texts(x_train)
x_train = tokenizer.texts_to_sequences(x_train)
x_test = tokenizer.texts_to_sequences(x_test)

print ('Encoding categorical target...')
lb_encoder = LabelEncoder()
y_train = lb_encoder.fit_transform(y_train)
y_test = lb_encoder.transform(y_test)

from keras.utils.np_utils import to_categorical

max_features = 795980
print('Pad sequences (samples x time)')
x_train = sequence.pad_sequences(x_train, maxlen=maxlen)
x_test = sequence.pad_sequences(x_test, maxlen=maxlen)
print('x_train shape:', x_train.shape)
print('x_test shape:', x_test.shape)
y_train = np.array(y_train)
y_test = np.array(y_test)

# Change target into matrix
y_train_cat = to_categorical(y_train)
y_test_cat = to_categorical(y_test)

model = Sequential()
model.add(Embedding(max_features, 128, input_length=maxlen))
model.add(Bidirectional(LSTM(100)))
model.add(Dropout(0.7))
model.add(Dense(y_train_cat.shape[1], activation='softmax'))

# try using different optimizers and different optimizer configs
model.compile(loss = 'categorical_crossentropy', optimizer = 'rmsprop',  metrics=['accuracy'])

print('Train...')
model.fit(x_train, y_train_cat,
          batch_size=batch_size,
          epochs=4,
          validation_data=[x_test, y_test_cat])

targets = ['BILL', 'CHANGE ENDORSEMENT', 'RETURNED CHECK', 'POLICY CHANGE']

# y_target_col = y.map(lambda x: x in targets)
# # Only consider 4 types
# x_target = x[y_target_col]
# y_target = y[y_target_col]

# Global vectorizer
vectorizer = TfidfVectorizer(sublinear_tf=True)
vectorizer.fit_transform(x)
x = vectorizer.transform(x)
result = {}
def get_model(result, name, clf):
    clf.fit(x, y)
    result[name] = clf

get_model(result, 'Rige Classifier', RidgeClassifier(tol=1e-3))
get_model(result, 'Passive-Aggresive Classifier', PassiveAggressiveClassifier(n_iter=100))
get_model(result, 'Perceptron', Perceptron(n_iter=100))
with open('ensemble_model.pkl', 'wb') as file:
    pickle.dump(result, file)
with open('vectorizer1.pkl', 'wb') as file:
    pickle.dump(vectorizer, file)
with open('vectorizer2.pkl', 'rb') as file:
    tokenizer = pickle.load(file)
with open('label_encoder.pkl', 'rb') as file:
    label_encoder = pickle.load(file)

y_target_col = y.map(lambda x: x in targets)
# Only consider 4 types
x_target = x[y_target_col]
y_target = y[y_target_col]

maxlen = 150
batch_size = 32
# Tokenizer
print ('Tokenize data...')
tokenizer = text.Tokenizer()
tokenizer.fit_on_texts(x_target)

# ======= test
x_temp = pd.Series('a3b334c6eefd be95012ebf2b')
x_temp_train = tokenizer.texts_to_sequences(x_temp)
x_target_train = tokenizer.texts_to_sequences(x_target)
# Encoding categorical target
print ('Encoding categorical target...')
lb_encoder = LabelEncoder()
y_target_train = lb_encoder.fit_transform(y_target)

# test =========
x_temp = x_target_train[1]
x_temp_len = len(x_temp)
for i in range(150-x_temp_len):
    x_temp.append(None)
 x_temp = x_temp[:150]
 x_temp = np.array(x_temp)
 x_temp = x_temp.reshape((1, 150))
 y_pred = model.predict(x_temp)
 index = np.argmax(y_pred)
 lb_encoder.inverse_transform(index)
 
 # encoder train and test
print y_target_train
with open('label_encoder.pkl', 'wb') as file:
    pickle.dump(lb_encoder, file)
# Pad sequences
print ('Pad sequences...')
max_features = 795980
print('Pad sequences (samples x time)')
x_target_train = sequence.pad_sequences(x_target_train, maxlen=maxlen)

# Change target into matrix
y_target_train_cat = to_categorical(y_target_train)

model = Sequential()
model.add(Embedding(max_features, 128, input_length=maxlen))
model.add(Bidirectional(LSTM(100)))
model.add(Dropout(0.7))
model.add(Dense(4, activation='softmax'))

# try using different optimizers and different optimizer configs
model.compile(loss = 'categorical_crossentropy', optimizer = 'rmsprop',  metrics=['accuracy'])

print('Train...')
model.fit(x_target_train, 
          y_target_train_cat,
          batch_size=batch_size,
          epochs=4)
