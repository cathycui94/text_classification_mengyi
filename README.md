# Text Classification

## (I) Demo (It might take several seconds to load.)
![](./demo/demo.gif)

## (II) Results
In this project, 4 different machine learning models were used to predict the type of document. Firstly, TfidfVectorizer was used to transform words into matrix, then train multiple classification models to find some with highest accuracy score and f1 score. 70% of the dataset is for train and 30% of the dataset is for test, here are some models with best results:

### 1. Ridge Model
![](./demo/d1.jpg) 
### 2. Perceptron Model
![](./demo/d2.jpg) 
### 3. Passive Aggressive
![](./demo/d3.jpg)
The average accuracy was about 86%, then a simple ensemble method was used, that is to use a majority vote approach to find the final result using these 3 models. 

Then we find that there are some types that are hard to classify. ‘BILL’ can easily be misclassified as ‘RETURNED CHECK’ or ’POLICY CHANGE’, ‘CHANGE ENDORSEMENT’ can easily be misclassified as ‘RETURNED CHECK’ or ’POLICY CHANGE ’. We then decide that if the document is classified as ‘RETURNED CHECK’ or ‘POLICY CHANGE’, we should use a LSTM model to find the right answer. We train a LSTM model and the result is below:

![](./demo/d4.jpg)

We can see that if we only consider above 4 types of document the accuracy would achieve 96%, then we can further improve our accuracy by adding an additional LSTM model.
